USE selfservice;
-- filas de ejemplo

INSERT INTO cliente (idCliente, nombre, apellido, correo, passwordShadow, passwordSalt, telefono)
VALUES
    -- Las contraseñas de todos los usuarios predef. serán 'secret'.
    --  Se les añade un 'salt' al final, antes de calcular su hash SHA2-256b
    ('julius','Juli','Mejía','gfa@de.alex',UNHEX(SHA2(CONCAT('secret', 'AAAA'),256)),'AAAA','971564528'),
    ('gabrielgbs97','Gabriel','Barceló','soy.el@puto.amo',UNHEX(SHA2(CONCAT('secret', 'AAAA'),256)),'AAAA','971120116'),
    ('alexgmp','Alexander','Medina','el@becario.es',UNHEX(SHA2(CONCAT('secret', 'AAAA'),256)),'AAAA','974265555');
    

-- Eliminado, no nos hace falta
-- INSERT INTO carta (idCarta, fechaActualizacion)
-- VALUES
    -- ('1', '2018/08/24 13:00:00'),
    -- ('2', '2018/08/24 13:00:00'),
    -- ('3', '2018/08/24 13:00:00'),
    -- ('4', '2018/08/24 13:00:00');

INSERT INTO producto (idProducto, tipo, nombreProducto, costeUnitario)
VALUES
    ('1', 'FRAPE', 'Café', '3.50'),
    ('2', 'FRAPE', 'Capuccino', '5.50'),
    ('3', 'BATIDO', 'Vainilla', '3.50'),
    ('4', 'BATIDO', 'Galleta', '4.50'),
    ('5', 'BATIDO', 'Plátano', '3.75'),
    ('6', 'CALIENTES', 'Café solo', '2.50'),
    ('7', 'CALIENTES', 'Café con leche', '3.50'),
    ('8', 'CALIENTES', 'Café cortado', '3.50');
    

INSERT INTO cafeteria (idCafeteria, nombreCafeteria, coordenadaNorte, coordenadaEste)
VALUES
    ('1', 'CoffeTake Aragón', '39.584920', '2.671548'), -- 39.584920, 2.671548
    ('2', 'CoffeTake Pere Garau', '39.574707', '2.663811'), -- 39.574707, 2.663811
    ('3', 'CoffeTake Font', '39.490909', '2.891669'), -- 39.490909, 2.891669
    ('4', 'CoffeTake Bandera', '39.493130', '2.890801'), -- 39.493130, 2.890801
    ('5', 'CoffeTake Major', '39.571365', '2.651432'), -- 39.571365, 2.651432
    ('6', 'CoffeTake General Riera', '39.583110', '2.646487'); -- 39.583110, 2.646487

INSERT INTO pedido (idPedido, fechaHora, fechaFinal, estadoPago, estado, idCliente, idCafeteria)
VALUES
    ('1', '2018/11/24 13:00:00', '2018/08/24 13:05:00', 'PAGADO', 'SERVIDO', 'gabrielgbs97', '1'),
    ('2', '2018/11/24 13:00:00', '2018/08/24 13:05:00', 'NOREVISADO', 'COLA', 'alexgmp', '1'),
    ('3', '2018/11/24 13:00:00', '2018/08/24 13:05:00', 'ERRORPAGO', 'LISTO', 'gabrielgbs97', '1'),
    ('4', '2018/11/24 13:00:00', '2018/08/24 13:05:00', 'ERRORPAGO', 'PREPARACION', 'gabrielgbs97', '2');

INSERT INTO cantidad (idPedido, idProducto, cantidad)
VALUES
    -- Cantidad pedido 1
    ('1', '1', '1'),
    ('1', '4', '3'),
    -- Cantidad pedido 2
    ('2', '1', '2'),
    -- Cantidad pedido 3
    ('3', '1', '1'),
    -- Cantidad pedido 4
    ('4', '1', '1');
    
INSERT INTO disponibilidad (idProducto, idCafeteria)
VALUES
    -- Disponibilidad producto 1:
    ('1','1'),
    ('1','2'),
    ('1','3'),
    ('1','4'),
    ('1','5'),
    ('1','6'),
    -- Disponibilidad producto 2:
    ('2','1'),
    ('2','2'),
    ('2','3'),
    ('2','4'),
    ('2','5'),
    ('2','6'),
    -- Disponibilidad producto 3:
    ('3','1'),
    ('3','2'),
    ('3','3'),
    ('3','4'),
    ('3','5'),
    ('3','6'),
    -- Disponibilidad producto 4:
    ('4','1'),
    ('4','2'),
    ('4','3'),
    ('4','4'),
    ('4','5'),
    ('4','6'),
    -- Disponibilidad producto 5:
    ('5','1'),
    ('5','2'),
    ('5','3'),
    ('5','4'),
    ('5','5'),
    ('5','6'),
    -- Disponibilidad producto 6:
    ('6','1'),
    ('6','2'),
    ('6','3'),
    ('6','4'),
    ('6','5'),
    ('6','6'),
    -- Disponibilidad producto 7:
    ('7','1'),
    ('7','2'),
    ('7','3'),
    ('7','4'),
    ('7','5'),
    ('7','6'),
    -- Disponibilidad producto 8:
    ('8','1'),
    ('8','2'),
    ('8','3'),
    ('8','4'),
    ('8','5'),
    ('8','6');
