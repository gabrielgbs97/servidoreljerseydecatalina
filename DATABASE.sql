-- Creamos base de datos llamada 'selfservice'
DROP DATABASE IF EXISTS selfservice;
CREATE DATABASE selfservice CHARACTER SET latin1 COLLATE latin1_spanish_ci;

-- Creamos usuario con acceso a la base de datos
DROP USER IF EXISTS selfservice@localhost;
CREATE USER selfservice@localhost IDENTIFIED BY 'ServiceSelf2018';
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, FILE, INDEX, ALTER, CREATE TEMPORARY TABLES, CREATE VIEW, EVENT, TRIGGER, SHOW VIEW, CREATE ROUTINE, ALTER ROUTINE, EXECUTE ON *.* TO selfservice@localhost REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
GRANT ALL PRIVILEGES ON selfservice.* TO selfservice@localhost;


USE selfservice;

CREATE TABLE cliente(
    idCliente VARCHAR(255) NOT NULL PRIMARY KEY,
    nombre VARCHAR(255) NOT NULL,
    apellido VARCHAR(255) NOT NULL,
    correo VARCHAR(255) NOT NULL,
    passwordShadow BINARY(32),
    passwordSalt BINARY(4),
    telefono VARCHAR(255) NOT NULL
);

CREATE TABLE producto(
    idProducto INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    tipo VARCHAR(255) NOT NULL,
    nombreProducto VARCHAR(255) NOT NULL,
    costeUnitario DOUBLE NOT NULL
);
    
-- CREATE TABLE carta(
    -- idCarta INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    -- fechaActualizacion DATETIME NOT NULL
-- );

CREATE TABLE cafeteria(
    idCafeteria INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombreCafeteria VARCHAR(255) NOT NULL,
    coordenadaNorte DOUBLE NOT NULL,
    coordenadaEste DOUBLE NOT NULL,
    horario VARCHAR(255) DEFAULT 'L-D: 8-23H'
    -- idCarta INT NOT NULL,
    -- FOREIGN KEY (idCarta) REFERENCES carta (idCarta)
);

CREATE TABLE pedido(
    idPedido INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    fechaHora DATETIME NOT NULL,
    fechaFinal DATETIME,
    estadoPago ENUM ('PAGADO','NOREVISADO','ERRORPAGO'),
    estado ENUM ('COLA','PREPARACION','LISTO','SERVIDO', 'CANCELADO'),
    idCliente VARCHAR(255) NOT NULL,
    idCafeteria INT NOT NULL,
    FOREIGN KEY (idCliente) REFERENCES cliente (idCliente),
    FOREIGN KEY (idCafeteria) REFERENCES cafeteria (idCafeteria)
);

CREATE TABLE cantidad(
    idPedido INT NOT NULL,
    idProducto INT NOT NULL,
    cantidad INT NOT NULL,
    PRIMARY KEY (idPedido,idProducto)
);

CREATE TABLE disponibilidad(
    idProducto INT NOT NULL,
    idCafeteria INT NOT NULL,
    PRIMARY KEY (idProducto, idCafeteria)
);
