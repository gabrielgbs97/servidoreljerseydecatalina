package com.selfservice.representacionBD;

import java.util.List;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.List;

public class ListaProductosUnaCafeteria {

	public List<Producto> productos;
	
	//Del resultado de la base de datos rellenamos una lista de objetos cafeterías
	public ListaProductosUnaCafeteria(ResultSet resultado) throws Exception {
		resultado.beforeFirst();
		productos = new ArrayList<Producto>();
		
		ResultSetMetaData resultadoMeta = resultado.getMetaData();
		System.out.println("Buscando cafeterías...");
		while(resultado.next()) {//Mientras hay filas
			Producto productoActual = new Producto();
			for(int i=1; i<=resultadoMeta.getColumnCount();i++) {
				
				//Nombre columna a revisar
				String target = resultadoMeta.getColumnName(i);
				if(target.equalsIgnoreCase("idProducto")) 
					productoActual.idProducto=resultado.getInt(i);
				else if(target.equalsIgnoreCase("tipo"))
					productoActual.tipo = resultado.getString(i);
				else if(target.equalsIgnoreCase("nombreProducto"))
					productoActual.nombreProducto =resultado.getString(i);
				else if(target.equalsIgnoreCase("costeUnitario"))
					productoActual.costeUnitario = resultado.getDouble(i);
			}
			productos.add(productoActual);
		}
	}
	
	public boolean hayProductos() {
		if (productos== null) {
			return false;
		}
		if(productos.isEmpty()) {
			System.out.println("No hay productos en cafeteria");
			return false;
		}
		
		return true;
	}
	
	//Dado un tipo de producto, devolver los productos de la lista del mismo tipo
	
	/*public Producto getProductoById(String tipo) {
		for (Producto producto : productos) {
			if (producto.tipo.contentEquals(tipo)) {
				return producto;
			}
		}
		return null;
	}*/
	
	public List<Producto> getProductosOfAKind(String tipo) {
		List<Producto> productosDeUnTipo = null;
		for (Producto producto : productos) {
			if (producto.tipo.contentEquals(tipo)) {
				productosDeUnTipo.add(producto);
			}
		}
		return productosDeUnTipo;
	}
	
	//Se obtiene un producto a partir de se identificador, para a�adir a un pedido.
	
	public Producto getProductoById(int idProducto) {
		for (Producto producto : productos) {
			if (producto.idProducto == idProducto ) {
				return producto;
			}
		}
		return null;
	}
}
