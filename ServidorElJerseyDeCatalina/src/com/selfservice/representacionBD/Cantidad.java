package com.selfservice.representacionBD;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.json.bind.annotation.JsonbTransient;

public class Cantidad {
	@JsonbTransient
	public int idPedido;
	
	public int idProducto, cantidad;
	
	public Cantidad() {
		
	}
	
	public Cantidad(int idPedido, int idProducto, int cantidad) {
		this.idPedido = idPedido;
		this.idProducto = idProducto;
		this.cantidad = cantidad;
	}
	
	
	public static List<Cantidad> getListaCantidad(ResultSet resultado) throws SQLException{
		resultado.beforeFirst();
		List<Cantidad> cantidades = new ArrayList<Cantidad>();
		
		ResultSetMetaData resultadoMeta = resultado.getMetaData();
		System.out.println("Buscando cantidad de productos en el pedido...");
		while(resultado.next()) {//Mientras hay filas
			Cantidad cantidadActual = new Cantidad();
			for(int i=1; i<=resultadoMeta.getColumnCount();i++) {
				
				//Nombre columna a revisar
				String target = resultadoMeta.getColumnName(i);
				if(target.equalsIgnoreCase("idPedido")) 
					cantidadActual.idPedido=resultado.getInt(i);
				
				else if(target.equalsIgnoreCase("idProducto"))
					cantidadActual.idProducto = resultado.getInt(i);
				else if(target.equalsIgnoreCase("cantidad"))
					cantidadActual.cantidad =resultado.getInt(i);
			}
			cantidades.add(cantidadActual);
		}
		return cantidades;
	}
	
	
	
}
