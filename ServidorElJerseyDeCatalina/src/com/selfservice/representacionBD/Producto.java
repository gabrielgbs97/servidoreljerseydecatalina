package com.selfservice.representacionBD;

public class Producto {
	public int idProducto;
	public String tipo, nombreProducto;
	public double costeUnitario;
	
	
	//Constructor vacio
	public Producto() {
		
	}
	
	public Producto(int idProducto, String tipo, String nombreProducto, double costeUnitario) {
		this.idProducto = idProducto;
		this.tipo = tipo;
		this.nombreProducto = nombreProducto;
		this.costeUnitario = costeUnitario;
	}
	
	
}
