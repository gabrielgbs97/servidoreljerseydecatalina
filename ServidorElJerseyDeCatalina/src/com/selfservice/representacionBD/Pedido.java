package com.selfservice.representacionBD;
import java.util.Date;
import java.sql.*;
import java.util.List;

import javax.json.bind.annotation.JsonbTransient;

public class Pedido {
	public int idPedido; //Primaria
	//public Cafeteria cafeteria; //Exterior
	public int idCafeteria; //Exterior
	public String idCliente;
	public java.util.Date fechaHora, fechaFinal;
	public String estadoPago, estado;
	
	public List<Cantidad> cantidades;
	
	public Pedido(){
		
	}
	
	public Pedido(int idPedido, int idCafeteria, String idCliente, Date fechaHora, Date fechaHoraFinal,
			String estadoPago, String estado) {
		this.idPedido = idPedido;
		this.idCafeteria = idCafeteria;
		this.idCliente = idCliente;
		this.fechaHora = fechaHora;
		this.fechaFinal = fechaHoraFinal;
		this.estadoPago = estadoPago;
		this.estado = estado;
	}
	
		public Pedido(ResultSet resultado) throws Exception {
			
			resultado.beforeFirst();
			if(resultado.next()) {
				ResultSetMetaData resultadoMeta = resultado.getMetaData();
				for(int i=1; i<=resultadoMeta.getColumnCount();i++) {
					String target = resultadoMeta.getColumnName(i);
					if(target.equalsIgnoreCase("idPedido"))
						idPedido=resultado.getInt(i);
					else if(target.equalsIgnoreCase("idCafeteria"))
						idCafeteria = resultado.getInt(i);
					else if(target.equalsIgnoreCase("idCliente"))
						idCliente = resultado.getString(i);
					else if(target.equalsIgnoreCase("fechaHora"))
						fechaHora = new Date(resultado.getTimestamp(i).getTime());
					else if(target.equalsIgnoreCase("fechaFinal"))
						fechaFinal = new Date(resultado.getTimestamp(i).getTime());
					else if(target.equalsIgnoreCase("estadoPago"))
						estadoPago =resultado.getString(i);
					else if(target.equalsIgnoreCase("estado"))
						estado = resultado.getString(i);
				}
			}
		}
		
		
		/***
		 * Devuelve verdadero si tiene algun campo vacío el cual debería estar escrito. Devuelve falso
		 * si todo es correcto.
		 * @return
		 */
		public boolean esVacio() {
			//Variables sin inicializar
			if (cantidades==null || idCliente==null || idCafeteria==0) {
				return true;
			}
			//Lista de cantidades vacía
			if (cantidades.isEmpty()) {
				return true;
			}
			
			//Lista de cantidades con cantidades a 0 o producto no válido 0.
			for(Cantidad cantidad: cantidades) {
				if(cantidad.cantidad == 0 || cantidad.idProducto==0)
					return true;
			}
			return false;
		}
	
}
