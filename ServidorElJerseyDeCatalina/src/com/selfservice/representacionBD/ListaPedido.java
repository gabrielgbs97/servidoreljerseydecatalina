package com.selfservice.representacionBD;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ListaPedido {
	
	public ArrayList<Pedido> pedidos;
	
	public ListaPedido() {
		
	}
	
	public ListaPedido (ResultSet resultado) throws Exception {
		resultado.beforeFirst();
		pedidos = new ArrayList<Pedido>();
		
		ResultSetMetaData resultadoMeta = resultado.getMetaData();
		System.out.println("Buscando pedidos...");
		while(resultado.next()) {//Mientras hay filas
			Pedido pedidoActual = new Pedido();
			for(int i=1; i<=resultadoMeta.getColumnCount();i++) {
				
				//Nombre columna a revisar
				String target = resultadoMeta.getColumnName(i);
				if(target.equalsIgnoreCase("idPedido")) 
					pedidoActual.idPedido=resultado.getInt(i);
				
				else if(target.equalsIgnoreCase("idCafeteria"))
					pedidoActual.idCafeteria = resultado.getInt(i);
				else if(target.equalsIgnoreCase("idCliente"))
					pedidoActual.idCliente =resultado.getString(i);
				else if(target.equalsIgnoreCase("fechaHora"))
					pedidoActual.fechaHora = new Date (resultado.getTimestamp(i).getTime());
				else if(target.equalsIgnoreCase("fechaHoraFinal"))
					pedidoActual.fechaFinal = new Date (resultado.getTimestamp(i).getTime());
				else if(target.equalsIgnoreCase("estadoPago"))
					pedidoActual.estadoPago =resultado.getString(i);
				else if(target.equalsIgnoreCase("estado"))
					pedidoActual.estado =resultado.getString(i);
			}
			pedidos.add(pedidoActual);
		}
	}

}
