package com.selfservice.representacionBD;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.List;

public class ListaCafeterias {
	public List<Cafeteria> cafeterias;
	
	//Del resultado de la base de datos rellenamos una lista de objetos cafeterías
	public ListaCafeterias(ResultSet resultado) throws Exception {
		resultado.beforeFirst();
		cafeterias = new ArrayList<Cafeteria>();
		
		ResultSetMetaData resultadoMeta = resultado.getMetaData();
		System.out.println("Buscando cafeterías...");
		while(resultado.next()) {//Mientras hay filas
			Cafeteria cafeteriaActual = new Cafeteria();
			for(int i=1; i<=resultadoMeta.getColumnCount();i++) {
				
				//Nombre columna a revisar
				String target = resultadoMeta.getColumnName(i);
				if(target.equalsIgnoreCase("idCafeteria")) 
					cafeteriaActual.idCafeteria=resultado.getInt(i);
				
				else if(target.equalsIgnoreCase("nombreCafeteria"))
					cafeteriaActual.nombreCafeteria = resultado.getString(i);
				else if(target.equalsIgnoreCase("horario"))
					cafeteriaActual.horario =resultado.getString(i);
				else if(target.equalsIgnoreCase("coordenadaNorte"))
					cafeteriaActual.coordenadaNorte =resultado.getDouble(i);
				else if(target.equalsIgnoreCase("coordenadaEste"))
					cafeteriaActual.coordenadaEste = resultado.getDouble(i);
			}
			cafeterias.add(cafeteriaActual);
		}
	}
	
	public boolean hayCafeterias() {
		//Por seguridad miramos que el puntero no referencia a null
		//Si no hay lista, no hay cafeterias. En condiciones normales no pasaría.
		//Siempre se construye una lista, almenos vacía (pero no apunta a null)
		if (cafeterias== null) {
			return false;
		}
		if(cafeterias.isEmpty()) {
			System.out.println("Lista cafeterias vacía");
			return false;
		}
		
		return true;
	}
	/***
	 * De la lista de cafaterias nos devuelve la cafetería con el idCafeteria especificado.
	 * Antes se tiene que haber conseguido la lsita de cafterias con el constructor.
	 * @param idCafeteria
	 * @return
	 */
	public Cafeteria getCafeteriaById(int idCafeteria) {
		for (Cafeteria cafeteria : cafeterias) {
			if (cafeteria.idCafeteria == idCafeteria) {
				return cafeteria;
			}
		}
		return null;
	}
}
