package com.selfservice.representacionBD;

import java.sql.*;
import java.util.Arrays;
import java.util.List;

import javax.json.bind.annotation.JsonbTransient;
import javax.xml.bind.DatatypeConverter;

import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.annotation.*;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Cliente {
	//Flag para ignorarlo en la conversión Json
	@JsonbTransient
	private byte[] passwordSalt;
	@JsonbTransient
	private byte[] passwordShadow;
	
	public String idCliente, nombre, apellido, correo, telefono;

	public String password;
	
	@JsonbTransient
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}

	public Cliente() {
		
	}
	
	public Cliente(String idCliente, String nombre, String apellido, String correo, String telefono,
		byte[] passwordShadow, byte[] passwordSalt) {
		this.idCliente = idCliente;
		this.nombre = nombre;
		this.apellido = apellido;
		this.correo = correo;
		this.telefono = telefono;
		this.passwordShadow = passwordShadow;
		this.passwordSalt = passwordSalt;
	}
	
	//Del resultado de la base de datos rellenamos el objeto cliente
	public Cliente(ResultSet resultado) throws Exception {
		
		resultado.beforeFirst();
		if(resultado.next()) {
			ResultSetMetaData resultadoMeta = resultado.getMetaData();
			for(int i=1; i<=resultadoMeta.getColumnCount();i++) {
				String target = resultadoMeta.getColumnName(i);
				if(target.equalsIgnoreCase("idCliente"))
					idCliente=resultado.getString(i);
				else if(target.equalsIgnoreCase("nombre"))
					nombre = resultado.getString(i);
				else if(target.equalsIgnoreCase("apellido"))
					apellido =resultado.getString(i);
				else if(target.equalsIgnoreCase("correo"))
					correo =resultado.getString(i);
				else if(target.equalsIgnoreCase("telefono"))
					telefono = resultado.getString(i);
				else if(target.equalsIgnoreCase("passwordShadow"))
					passwordShadow = resultado.getBytes(i);
				else if(target.equalsIgnoreCase("passwordSalt"))
					passwordSalt = resultado.getBytes(i);
			}
		}
	}
	
	@Override
	public String toString() {
		String str;
		str =""+
				this.idCliente+"\n"+
		this.nombre+"\n"+
		this.apellido+"\n"+
		this.correo+"\n"+
		this.telefono+"\n";
		if(passwordShadow != null) {
			str+=DatatypeConverter.printHexBinary(passwordShadow)+"\n";
		}
		if(passwordSalt != null) {
			str+=DatatypeConverter.printHexBinary(passwordSalt)+"\n";
		}		
		return str;
	}
	/**
	 * Retorna verdadero si el se obtiene el hash almacenado.
	 * @param password
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public boolean pruebaPasword(String password) throws UnsupportedEncodingException, NoSuchAlgorithmException {
		byte[] saltedPass;
		MessageDigest hash = MessageDigest.getInstance("SHA-256");
		saltedPass = this.saltPassword(password, passwordSalt);
		System.out.println("--- Starting password test ---");
		System.out.println("Salted given pass HEX :"+DatatypeConverter.printHexBinary(saltedPass));
		System.out.println("Calculated salted given pass digest HEX :"+DatatypeConverter.printHexBinary(hash.digest(saltedPass)));
		System.out.println("Stored digest HEX: "+DatatypeConverter.printHexBinary(passwordShadow));
		return MessageDigest.isEqual(
				hash.digest(saltedPass),
				passwordShadow
				);
	}
	
	//El password aleatorizado será tipo: [Less Significant Bit] -> "password"+salt -> [Most Significant Bit]
	public static byte[] saltPassword(String password, byte[] salt) throws UnsupportedEncodingException {
		//TODO: arreglar
		byte[] passToCheckBytes = password.getBytes("UTF-8");
		System.out.println("Checking pass: "+password);
		byte[] saltedPass = new byte[passToCheckBytes.length+salt.length];
		
		for(int i=0; i<saltedPass.length; i++) {
			
			if(i<passToCheckBytes.length) {
				
				saltedPass[i] = passToCheckBytes[i];
			}
			else
				saltedPass[i] = salt[i-passToCheckBytes.length];
		}
		
		return saltedPass;
	}
	
	public boolean esVacio() {
		return idCliente==null;
	}
	
	public boolean formatoCorrecto() {
		return password.length()>1 &&
				idCliente.length()>1 &&
				nombre.length()>1 &&
				apellido.length()>1 &&
				correo.length()>1 &&
				telefono.length()>1;
				
		
	}
	
}
