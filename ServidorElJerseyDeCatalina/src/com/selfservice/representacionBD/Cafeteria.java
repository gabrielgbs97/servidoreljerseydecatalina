package com.selfservice.representacionBD;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.List;

import javax.json.bind.annotation.JsonbTransient;

public class Cafeteria {
	public int idCafeteria;
	public String nombreCafeteria, horario;
	public double coordenadaNorte, coordenadaEste;
	
	
	public List<Producto> productos;
	
	// Constructor vacío. Estrategia: Crearemos una instancia vacía para luego modificar sus campos
	// públicos
	public Cafeteria() {
		//No ejecutar código aquí
	}
	
	public Cafeteria(int idCafeteria, String nombreCafeteria, String horario, double coordenadaNorte,
			double coordenadaEste, List<Producto> productos) {
		this.idCafeteria = idCafeteria;
		this.nombreCafeteria = nombreCafeteria;
		this.horario = horario;
		this.coordenadaNorte = coordenadaNorte;
		this.coordenadaEste = coordenadaEste;
		this.productos = productos;
	}
	
	/***
	 * TODO
	 * Rellena la lista de productos de esta cafetería.
	 */
	public void listarProductos() {
		
	}
	
}
