package com.selfservice.servidor;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.selfservice.gestionBD.GestorBD;
import com.selfservice.gestionBD.GestorTokens;
import com.selfservice.representacionBD.ListaPedido;

@Path("/pedidos")
public class PedidosResource {
	public static final JsonbConfig jsonbConfig = new JsonbConfig()
    	    .withNullValues(false);
    static GestorBD baseDatos;
    static GestorTokens gestorTokens;
    
    public PedidosResource() {
		try {
			if (baseDatos == null)
				baseDatos = new GestorBD();
		} catch (Exception e) {
			System.out.println("ERROR BASE DE DATOS\n");
			e.printStackTrace();
		}
		if (gestorTokens==null) {
			gestorTokens= new GestorTokens();
		}
    }
	
    /***
     * Recurso para modificar pedidos en 'batch'. Le pasamos una cadena json de pedidos, con algunos
     * campos de pedidos: claves, "idPedido","fechaFinal" y "estado".
     * @param idCafeteria
     * @return
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON+"; charset=UTF-8")
    public Response updatePedidos(String cuerpoJson) {
    	Jsonb jsonb = JsonbBuilder.create(jsonbConfig);
    	System.out.println("Payload recibido: "+cuerpoJson);
    	ListaPedido pedidos= jsonb.fromJson(cuerpoJson, ListaPedido.class);
    	int nPedidosCambiados = 0;
    	try {
    		nPedidosCambiados=GestorBD.updatePedidos(pedidos);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(500).build();
		}
    	return Response.status(200).entity("{\"cambios\":"+nPedidosCambiados+"}").build();
    	
    }
}
