package com.selfservice.servidor;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.selfservice.gestionBD.EstadoValidacion;
import com.selfservice.gestionBD.GestorBD;
import com.selfservice.gestionBD.GestorTokens;
import com.selfservice.representacionBD.ListaCafeterias;
import com.selfservice.representacionBD.ListaPedido;
import com.selfservice.representacionBD.ListaProductosUnaCafeteria;
import com.selfservice.representacionBD.Pedido;


@Path("/restaurants")
public class CafeteriasResource {
	public static final JsonbConfig jsonbConfig = new JsonbConfig()
    	    .withNullValues(false);
    static GestorBD baseDatos;
    static GestorTokens gestorTokens;
    
    public CafeteriasResource() {
		try {
			if (baseDatos == null)
				baseDatos = new GestorBD();
		} catch (Exception e) {
			System.out.println("ERROR BASE DE DATOS\n");
			e.printStackTrace();
		}
		if (gestorTokens==null) {
			gestorTokens= new GestorTokens();
		}
    }
    
    /***
     * Devuelve una cadena de cafeterías disponibles
     * @return
     */
    
    @GET
    @Produces(MediaType.APPLICATION_JSON+"; charset=UTF-8")
    public Response getCafeterias(@Context HttpHeaders httpHeaders) {
    	//Verificamos el token de autenticación del cliente. Si no es valido
    	//devolvemos '401 No autorizado'
    	EstadoValidacion estado = GestorTokens.verificarToken(httpHeaders);
    	if(estado.validez()!=EstadoValidacion.VALIDO)
    		return Response.status(401).build();
    	try {
			ListaCafeterias cafeterias = GestorBD.getListaCafeterias();
			//Si no hay cafeterias se dev8elve codigo 404 NOT FOUND
			if(!cafeterias.hayCafeterias())
				return Response.status(404).build();
			
			System.out.println("Hay cafeterías");
			//Si todo ha ido bien, devolvemos lista de cafeterias
			return Response.status(200).entity(cafeterias).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(500).build();
		}
    }
    
    
    @GET
    @Path("/{idCafeteria}/productos")
    @Produces(MediaType.APPLICATION_JSON+"; charset=UTF-8")
    public Response getProductos(@PathParam("idCafeteria") int idCafeteria, @Context HttpHeaders httpHeaders) {
    	
    	EstadoValidacion estado = GestorTokens.verificarToken(httpHeaders);
    	if(estado.validez()!=EstadoValidacion.VALIDO)
    		return Response.status(401).build();
    	try {
    		ListaProductosUnaCafeteria productos = GestorBD.getProductosDeCafeteria(idCafeteria);
    		if(!productos.hayProductos())
				return Response.status(404).build();
    		
			//Si todo ha ido bien, devolvemos lista de productos
			return Response.status(200).entity(productos).build();
    		
    	} catch (Exception e) {
			e.printStackTrace();
			return Response.status(500).build();
		}
    }
    /***
     * Para conseguir todos los pedidos de una cafetería, para la pantalla de cocina
     * @param idCafeteria
     * @return
     */
    @GET
    @Path("/{idCafeteria}/pedidos")
    @Produces(MediaType.APPLICATION_JSON+"; charset=UTF-8")
    public Response getPedidos(@PathParam("idCafeteria") int idCafeteria) {

    	try {
    		ListaPedido pedidos = GestorBD.getPedidoByIdCafeteria(idCafeteria);
    		
			//Si todo ha ido bien, devolvemos lista de productos
			return Response.status(200).entity(pedidos).build();
    		
    	} catch (Exception e) {
			e.printStackTrace();
			return Response.status(500).build();
		}
    }
    

    	
}
