package com.selfservice.servidor;


import java.sql.SQLException;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.selfservice.gestionBD.*;

@Path("/token")
public class TokenResource {
	static GestorBD baseDatos;
	static GestorTokens fabricaTokens;
	
	public TokenResource() {
		try {
			if (baseDatos == null)
				baseDatos = new GestorBD();
		} catch (Exception e) {
			System.out.println("ERROR BASE DE DATOS\n");
			e.printStackTrace();
		}
		if(fabricaTokens==null) {
			fabricaTokens = new GestorTokens();
		}
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON+"; charset=UTF-8")
	public Response getToken(@Context HttpHeaders httpheaders) {
		String authHeader = httpheaders.getHeaderString("Authorization");
		System.out.println(authHeader);
		EstadoValidacion estado;
		
		try {
			String token;
			//Log in valido ?
			estado = baseDatos.clienteLogIn(authHeader);
			/**
			 * EMPEZAMOS CODIGO DE PRUEBA, HACE UN FUNCIONAMIENTO FALSO
			 */
			//Creamos estado 'a mano'
			//estado = new EstadoValidacion(EstadoValidacion.VALIDO, "gabrielgbs97");
			/**
			 * TERMINAMOS CODIGO DE PRUEBA, HACÃ�A UN FUNCIONAMIENTO FALSO
			 */
			
			if (estado.validez() == EstadoValidacion.VALIDO) {
				System.out.println("Emitiendo TOKEN JWT...");
				return Response.status(200).entity("{\"token\":"+
						"\""+fabricaTokens.emitirToken(estado.getIdUsuario())+
						"\"}").build();
			}
			else
				return Response.status(401).build();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).build();
		}
		catch (Exception e) {
			e.printStackTrace();
			return Response.status(500).build();
		}	
	}
}
