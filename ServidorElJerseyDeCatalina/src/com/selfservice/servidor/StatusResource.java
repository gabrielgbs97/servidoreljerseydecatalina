package com.selfservice.servidor;

import javax.json.Json;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;


@Path("/status")

public class StatusResource {
	JsonObject status;
	
	public StatusResource() {
		JsonBuilderFactory factory = Json.createBuilderFactory(null);
		status = factory.createObjectBuilder()
						.add("status","PARTIAL RUNNING")
				.build();
	}
	
    @GET
    @Produces(MediaType.APPLICATION_JSON+"; charset=UTF-8")
    public Response sendStatus() {
    	return Response.status(200).entity(status.toString()).build();
    }
    
}