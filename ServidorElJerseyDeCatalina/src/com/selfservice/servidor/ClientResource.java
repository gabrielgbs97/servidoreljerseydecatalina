package com.selfservice.servidor;

import com.selfservice.gestionBD.EstadoValidacion;
import com.selfservice.gestionBD.GestorBD;
import com.selfservice.gestionBD.GestorTokens;
import com.selfservice.representacionBD.Cliente;
import com.selfservice.representacionBD.ListaPedido;
import com.selfservice.representacionBD.ListaProductosUnaCafeteria;
import com.selfservice.representacionBD.Pedido;

import java.sql.SQLException;

import javax.json.JsonObject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;


@Path("/accounts")

public class ClientResource{
    public static final JsonbConfig jsonbConfig = new JsonbConfig()
    	    .withNullValues(false);
    static GestorBD baseDatos;
    static GestorTokens gestorTokens;
    
	public ClientResource() {
		try {
			if (!GestorBD.hayConexion())
				baseDatos = new GestorBD();
		} catch (Exception e) {
			System.out.println("ERROR BASE DE DATOS\n");
			e.printStackTrace();
		}
		if (gestorTokens==null) {
			gestorTokens= new GestorTokens();
		}
		
	}
	
    @GET
    @Path("/{idCliente}")
    @Produces(MediaType.APPLICATION_JSON+"; charset=UTF-8")
    public Response getClient(@PathParam("idCliente") String idCliente, @Context HttpHeaders headers) {
    	//Implementacion de prueba, sin buscar en base de datos
//    	Cliente cliente = new Cliente("gabrielgbs97", "Gabriel", "Barceló", "sonicare@as.xd", "971120116",
//    			"HASH".getBytes(), "HASH".getBytes());
    	
    	
		EstadoValidacion estado = GestorTokens.verificarToken(headers);
		//Si el token es valido para el cliente dado
		if(estado.validez(idCliente)) {
			Cliente cliente;
			try {
				cliente = GestorBD.getClienteById(idCliente);
			}catch (Exception e) {
				e.printStackTrace();
				return Response.status(500).build();
			}
			//Si el cliente dado existe (significa que hemos obtenido un
			//cliente no vacio. Si no existe devolvemos '401 No encontrado'
			//El primer caso no debería darse en una situación normal.
			if(cliente.esVacio()) {
				return Response.status(404).build();
			}
			//En caso contrario, devolvemos el cliente en forma JSON
			else {
				return Response.status(200).entity(cliente).build();
			}
		}else {
			return Response.status(401).build();
		}
    	
    }
    
    @POST
    @Path("/newAccount")
    @Consumes(MediaType.APPLICATION_JSON)
    //En cuerpoJson estará el cuerpo del HTTP POST
    public Response registerClient(String cuerpoJson ) {
    	Jsonb jsonb = JsonbBuilder.create(jsonbConfig);
    	
    	System.out.println("Payload recibido: "+cuerpoJson);
    	
    	Cliente cliente = jsonb.fromJson(cuerpoJson, Cliente.class);
    	System.out.println("Cliente deserializado: \n"+cliente.toString());
    	if(cliente.formatoCorrecto()) {
    		try {
				if(GestorBD.insertarNuevoCliente(cliente)) {
					return Response.status(200).build();
				}else
					//Si ya existe el cliente
					return Response.status(409).build();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return Response.status(500).build();
			}
    	}
		return Response.status(200).build();
    }
    
    @GET
    @Path("/{idCliente}/pedidos/{idPedido}")
    @Produces(MediaType.APPLICATION_JSON+"; charset=UTF-8")
    public Response getPedido(@PathParam("idCliente") String idCliente, @PathParam("idPedido") int idPedido, @Context HttpHeaders httpHeaders) {
    	System.out.println("Buscando pedido....");
    	
    	Pedido pedido;
    	EstadoValidacion estado = GestorTokens.verificarToken(httpHeaders);
    	if(estado.validez()!=EstadoValidacion.VALIDO)
    		return Response.status(401).build();
    	try {
    		if(estado.validez(idCliente)){
    			pedido = GestorBD.getOrderById(idPedido, idCliente);
    			if(pedido.esVacio())
    				return Response.status(404).build();
    		
    			return Response.status(200).entity(pedido).build();
    		
    		}else {
				return Response.status(401).build();
			}
    			
    	} catch (Exception e) {
			e.printStackTrace();
			return Response.status(500).build();
		}
    }
    
    @GET
    @Path("/{idCliente}/pedidos")
    @Produces(MediaType.APPLICATION_JSON+"; charset=UTF-8")
    public Response getPedidos(@PathParam("idCliente") String idCliente,
    		@Context HttpHeaders httpHeaders) {
    	
		EstadoValidacion estado = GestorTokens.verificarToken(httpHeaders);
		//Si el token es valido para el cliente dado
		if(estado.validez(idCliente)) {
			ListaPedido listaPedido;
			try {
				listaPedido = GestorBD.getPedidosByIdCliente(idCliente);
			}
			catch (Exception e) {
				e.printStackTrace();
				return Response.status(500).build();
			}
			return Response.status(200).entity(listaPedido).build();
		}else {
			return Response.status(401).build();
		}
    }
    
    @POST
    @Path("/{idCliente}/pedidos/newPedido")
    @Produces(MediaType.APPLICATION_JSON+"; charset=UTF-8")
    public Response postPedido(@PathParam("idCliente") String idCliente,
    		@Context HttpHeaders httpHeaders, String cuerpoJson ) {
		EstadoValidacion estado = GestorTokens.verificarToken(httpHeaders);
		//Si el token es valido para el cliente dado
		if(estado.validez(idCliente)) {
			Jsonb jsonb = JsonbBuilder.create(jsonbConfig);
	    	System.out.println("Payload recibido: "+cuerpoJson);
	    	Pedido pedido= jsonb.fromJson(cuerpoJson, Pedido.class);
	    	System.out.println("Pedido reserializado:\n"+jsonb.toJson(pedido));
	    	//Revisamos que pedido es correcto
	    	if(pedido.esVacio()) {
	    		return Response.status(Status.BAD_REQUEST).build();
	    	}
			//Miramos que el pedido va dirigido al usuario autorizado
			if(estado.validez(pedido.idCliente)) {
				try {
					//Insertamos el pedido en la base de datos
					GestorBD.insertarNuevoPedido(pedido);
					return Response.status(201).build();
					//Petición no válida, seguramente dejó campos vacíos en el JSON
				}
				catch (Exception e) {
					e.printStackTrace();
					return Response.status(500).build();
				}
			}
			else {//En el pedido ha puesto la idCliente de otro usuario
				return Response.status(401).build();
			}
		}
		else {//Si el token no es valido para el cliente dado se prohíbe el acceso
			return Response.status(401).build();
		}
    }
    
    
}
