package com.selfservice.gestionBD;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
//IMPORTS
import java.sql.*;
import java.util.List;
import java.util.Random;
import java.util.Random.*;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;

import com.selfservice.representacionBD.Cafeteria;
import com.selfservice.representacionBD.Cantidad;
import com.selfservice.representacionBD.Cliente;
import com.selfservice.representacionBD.ListaCafeterias;
import com.selfservice.representacionBD.ListaPedido;
import com.selfservice.representacionBD.ListaProductosUnaCafeteria;
import com.selfservice.representacionBD.Pedido;

import org.mariadb.jdbc.*;
import org.glassfish.jersey.internal.util.Base64;

public class GestorBD {
	private static Connection connect = null;
	
	private static String direccionBaseDatos =
			"jdbc:mariadb://localhost/selfservice";
	
	@SuppressWarnings("deprecation")
	public GestorBD() throws Exception{
		/***
		 * NO FUNCIONA SIN LA PINCHE WEA DE ABAJAO (IGNORAR WARNING)
		 */
		Class.forName("org.mariadb.jdbc.Driver").newInstance();
		/***
		 * NO FUNCIONA SIN LA PINCHE WEA DE ARRIBA(IGNORAR WARNING)
		 */
		if(connect == null)
			connect = DriverManager.getConnection(direccionBaseDatos,
					"selfservice",
					"ServiceSelf2018");
		System.out.println("Conexion base datos OK");
	}
	
	public static Cliente getClienteById(String idCliente) throws SQLException, Exception {
		PreparedStatement peticion =
				connect.prepareStatement(SentenciaPreparada.CLIENTE_POR_ID.toString());
		peticion.setString(1, idCliente);
		return new Cliente(peticion.executeQuery());
	}
	
	public EstadoValidacion clienteLogIn(String authHeader) throws SQLException, Exception {
        if (authHeader != null) {
        	Cliente cliente = new Cliente();
            // Recortamos el "Basic " Se puede eprfilar más esta función
            byte[] e = Base64.decode(authHeader.substring(6).getBytes());
            String usuarioYPassword = new String(e);
            // *****Split the username from the password*****
            String usuario = usuarioYPassword.substring(0, usuarioYPassword.indexOf(":"));
            String password = usuarioYPassword.substring(usuarioYPassword.indexOf(":") + 1);
            // check username and password
            cliente=getClienteById(usuario);
            System.out.println(cliente.toString());
            //Si existe el cliente pedido
            if(!cliente.esVacio()) {
            	if(cliente.pruebaPasword(password))
            		return new EstadoValidacion(1,usuario);
            }
        }
		return new EstadoValidacion(EstadoValidacion.NO_VALIDO,null);
	}
	
	public static boolean insertarNuevoCliente(Cliente cliente) throws Exception {
		if(yaExiste(cliente))
			return false;
		
		//Númnero aleaorio para aleatorizar el password
		byte[] salt = new byte[4];
		System.out.println("enerando numero aleatorio...");
		//salt = "aaaa".getBytes(); //Salt no aleatorio, usado antes para probar.
		//SecureRandom.getInstanceStrong().nextBytes(salt); //Muy seguro pero muy lento (~70s) 'el más aleatorio'
		new Random().nextBytes(salt); //Pseudoaleatorio, viejo, rapido, menos seguro.
		
		//Campos: (idCliente, nombre, apellido, correo, telefono, passwordShadow, passwordSalt)
		System.out.println("Insertando fila en la base de datos...");
		PreparedStatement peticion = connect.prepareStatement(SentenciaPreparada.INSERTAR_CLIENTE.toString());
		peticion.setString(1, cliente.idCliente);
		peticion.setString(2, cliente.nombre);
		peticion.setString(3, cliente.apellido);
		peticion.setString(4, cliente.correo);
		peticion.setString(5, cliente.telefono);
		MessageDigest hash = MessageDigest.getInstance("SHA-256");
		peticion.setBytes(6,
				hash.digest(
						Cliente.saltPassword(cliente.password,
								salt)
						)
				);
		peticion.setBytes(7, salt);
		
		peticion.executeUpdate();
		return true;
		
	}
	
	public static void insertarNuevoPedido(Pedido pedido) throws Exception {
		//Añadimos comportamiento aleatorio para simular el proceso
		double i = new Random().nextDouble();
		
		PreparedStatement peticion = connect.prepareStatement(
				SentenciaPreparada.INSERTAR_PEDIDO.toString(),
				Statement.RETURN_GENERATED_KEYS);
		
		java.util.Date dt = new java.util.Date();
		//Fijamos fecha de ahora para el pedido efectuado
		peticion.setTimestamp(1, new java.sql.Timestamp(dt.getTime()));
		
		/**
		 * SIMULAMOS VERIFICACIÓN DE PAGO
		 */
		if (i > 0.80) {
			//Se denegará el pago el 20% de las veces
			peticion.setString(3, "ERRORPAGO");
			peticion.setString(4, "COLA");
			peticion.setTimestamp(2, new java.sql.Timestamp(dt.getTime()));
			
		}
		/**
		 * SIMULAMOS COMPORTAMIENTO DEL PEDIDO PAGADO
		 */
		else {
			i = new Random().nextDouble();
			double minutosEspera = 1000*60*15*new Random().nextDouble();//Resulta entre 0-15min
			peticion.setString(3, "PAGADO");
			//Varios casos posibles simulados: COLA, PREPARACIÓN, LISTO, SERVIDO
			if (i<= (1/4.0)) {
				peticion.setDate(2, new java.sql.Date(dt.getTime()));
				peticion.setString(4, "COLA");
			}
			else if(i>(1/4.0) && i <= (2/4.0)) {
				peticion.setTimestamp(2, new java.sql.Timestamp(dt.getTime()+(long)(minutosEspera)));
				peticion.setString(4, "PREPARACION");
			}
			else if(i>(2/4.0) && i <= (3/4.0)) {
				peticion.setTimestamp(2, new java.sql.Timestamp(dt.getTime()+(long)(minutosEspera)));
				peticion.setString(4, "LISTO");
			}
			else if(i>(3/4.0)) {
				peticion.setTimestamp(2, new java.sql.Timestamp(dt.getTime()+(long)(minutosEspera)));
				peticion.setString(4, "SERVIDO");
			}
		}//Fin simulación comportamiento pedido
		
		peticion.setInt(5, pedido.idCafeteria);
		peticion.setString(6, pedido.idCliente);
		
		/**
		 * Hemos ajustado el pedido, ahora falta insertar la cantidad
		 */
		
		
		//Conseguimos el idPedido del pedido insertado
		int filasAfectadas = peticion.executeUpdate();
		int idPedidoInsertado=0;
		if(filasAfectadas==1) {
			ResultSet rs = peticion.getGeneratedKeys();
			if(rs.next())
				idPedidoInsertado = rs.getInt(1);
			
			insertarCantidades(idPedidoInsertado, pedido.cantidades);
		}
		
		
	}
	/**
	 * Insertamos una lista de cantidad de un pedido dado
	 * @param idPedido
	 * @param cantidades
	 * @throws Exception
	 */
	private static void insertarCantidades(int idPedido, List<Cantidad> cantidades) throws Exception {
		PreparedStatement peticionCantidad = connect.prepareStatement(
				SentenciaPreparada.INSERTAR_CANTIDAD.toString());
		for(Cantidad cantidad: cantidades) {
			peticionCantidad.setInt(1, idPedido);
			peticionCantidad.setInt(2, cantidad.idProducto);
			peticionCantidad.setInt(3, cantidad.cantidad);
			
			peticionCantidad.addBatch();
		}
		peticionCantidad.executeBatch();
	}
	
	//antes de usar esta función comprobar q el cliente es corrreco con cliente.formatoCorrecto()
	public static boolean yaExiste(Cliente cliente) throws SQLException, Exception {
		Cliente clienteAux = getClienteById(cliente.idCliente);
		if(clienteAux.idCliente==null) {
			return false;
		}
		
		return clienteAux.idCliente.equalsIgnoreCase(cliente.idCliente);
	}
	
	public static boolean hayConexion() {
		if(connect==null) {
			return false;
		}
		try {
			if(!connect.isClosed()) {
				PreparedStatement prueba = connect.prepareStatement("SHOW TABLES");
				prueba.executeQuery();
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return false;
	}
	
	public static ListaCafeterias getListaCafeterias() throws Exception {
		PreparedStatement peticionCafeterias = connect.prepareStatement(SentenciaPreparada.CAFETERIAS.toString());
		ResultSet resultado = peticionCafeterias.executeQuery();
		ListaCafeterias listaCafeterias = new ListaCafeterias(resultado);
//		Imprimir cafaterías obtenidas
//		JsonbConfig config = new JsonbConfig()
//				.withFormatting(true)
//				.withNullValues(false);
//		Jsonb jsonb = JsonbBuilder.create(config);
//		String s = jsonb.toJson(listaCafeterias);
//		System.out.println(s);
		return listaCafeterias;
	}
	
	/*****
	 * Obtener los productos de una cafeteria.
	 */
	
	public static ListaProductosUnaCafeteria getProductosDeCafeteria(int idCafeteria)  throws SQLException, Exception {
		
		PreparedStatement peticionProductos = connect.prepareStatement(SentenciaPreparada.PRODUCTO_POR_IDCAFETERIA.toString());
		peticionProductos.setInt(1, idCafeteria);
		ResultSet resultado = peticionProductos.executeQuery();
		
		ListaProductosUnaCafeteria listaProductosUnaCafeteria = new ListaProductosUnaCafeteria(resultado);
		return listaProductosUnaCafeteria;
	}
	
	public static Pedido getOrderById(int idPedido, String idCliente) throws SQLException, Exception {
		Pedido pedido;
		// Obtener una petici�n en concreto de un cliente.
		PreparedStatement peticion = connect.prepareStatement(SentenciaPreparada.PEDIDO_POR_ID.toString());
		peticion.setInt(1, idPedido);
		peticion.setString(2, idCliente);
		// Obtener las cantidades de los productos de un pedido.
		PreparedStatement peticionCantidad = connect.prepareStatement(SentenciaPreparada.CANTIDAD_POR_IDPEDIDO.toString());
		peticionCantidad.setInt(1, idPedido);
		pedido = new Pedido(peticion.executeQuery());
		
		pedido.cantidades = Cantidad.getListaCantidad(peticionCantidad.executeQuery());
		return pedido;
	}
	
	public static ListaPedido getPedidosByIdCliente(String idCliente) throws Exception {
		ListaPedido pedidos;
		PreparedStatement peticion = connect.prepareStatement(
				SentenciaPreparada.PEDIDO_POR_IDCLIENTE.toString());
		PreparedStatement peticionCantidad = connect.prepareStatement(
				SentenciaPreparada.CANTIDAD_POR_IDPEDIDO.toString());
		peticion.setString(1, idCliente);
		pedidos = new ListaPedido(peticion.executeQuery());

		for(Pedido pedido: pedidos.pedidos) {
			peticionCantidad.setInt(1, pedido.idPedido);
			pedido.cantidades =
					Cantidad.getListaCantidad(peticionCantidad.executeQuery());
		}
		return pedidos;
	}
	
	public static ListaPedido getPedidoByIdCafeteria(int idCafeteria) throws Exception {
		ListaPedido pedidos;
		PreparedStatement peticion = connect.prepareStatement(
				SentenciaPreparada.PEDIDO_POR_IDCAFETERIA.toString());
		PreparedStatement peticionCantidad = connect.prepareStatement(
				SentenciaPreparada.CANTIDAD_POR_IDPEDIDO.toString());
		peticion.setInt(1, idCafeteria);
		pedidos = new ListaPedido(peticion.executeQuery());
		for(Pedido pedido: pedidos.pedidos) {
			peticionCantidad.setInt(1, pedido.idPedido);
			pedido.cantidades =
					Cantidad.getListaCantidad(peticionCantidad.executeQuery());
		}
		return pedidos;
	}

	public static int updatePedidos(ListaPedido pedidos) throws Exception {
		PreparedStatement peticion = connect.prepareStatement(
				SentenciaPreparada.ACTUALIZAR_PEDIDO.toString());
		
		java.util.Date dt = new java.util.Date();
		
		for (Pedido pedido : pedidos.pedidos) {
			peticion.setTimestamp(1, new java.sql.Timestamp(dt.getTime()));
			peticion.setString(2, pedido.estado);
			peticion.setInt(3, pedido.idPedido);
			System.out.println("Cambiando pedido("+pedido.idPedido+"): "+pedido.estado);
			peticion.addBatch();
		}
		int [] filasCambiadas = peticion.executeBatch();
		if (filasCambiadas.length==0) {
			return 0;
		}
		int total = 0;
		for(int cambio: filasCambiadas)
			total+=cambio;
		
		return total;
		
	}
	
}
