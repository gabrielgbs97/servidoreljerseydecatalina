package com.selfservice.gestionBD;

import com.auth0.jwt.algorithms.*;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import javax.ws.rs.core.HttpHeaders;
import java.util.Date;

import com.auth0.jwt.*;

public class GestorTokens {
	static final long DURACION_TOKEN = 1000*60*24*7; //En milisegundos;
	static private Algorithm algoritmoAutenticacion = Algorithm.HMAC256("secret");
	static private JWTVerifier validador;
	
	/**
	 * La clase verificará y emitirá los JWT
	 * @param algoritmo Algoritmo de autenticación o firma a utilizar
	 */
	public GestorTokens() {
		if (validador==null) {
			validador = JWT.require(algoritmoAutenticacion)
			        .withIssuer("SelfService")
			        .build(); //Reusable verifier instance
		}

	}
	
	/***
	 * Asigna el parámetro idCliente al campo 'subject', firma y devuelve el token.
	 * @param idCliente
	 * @return
	 */
	public String emitirToken(String idCliente) {
		String token = JWT.create()
		        .withIssuer("SelfService")
		        .withSubject(idCliente)
		        .withExpiresAt(GestorTokens.getExpirationDate())
		        .sign(algoritmoAutenticacion);
		
		return token;
	}
	
	/***
	 * Emitiremos un objeto EstadoValidacion que indique si es valido el token y que usuario autentica.
	 * Si no es vàlido o ha expirado el usuario autenticado es null.
	 * @param token String que representa el token JWT
	 * @return
	 */
	public static EstadoValidacion verificarToken(HttpHeaders httpHeaders) {
		//Token no valido hasta que se demuestre lo contrario
		EstadoValidacion estado = new EstadoValidacion(
				EstadoValidacion.NO_VALIDO,
				null
				);
		//Obtenemos el parámetro de auth., tipo: 'Authorization: Bearer BASE64JWT'
		String authHeader = httpHeaders.getHeaderString("Authorization");
		//Si no hay cabecera de autorización. Autorización no valida:
		if (authHeader==null) {
			return estado;
		}
		
		String token = authHeader.substring(7);
		System.out.println("Token a verificar: "+token);

		try {
			//Si el token no es valido saltará excepción
		    DecodedJWT jwt = validador.verify(token);

		    estado = new EstadoValidacion(EstadoValidacion.VALIDO,
		    		jwt.getSubject());
		    
		}
		catch (com.auth0.jwt.exceptions.TokenExpiredException exception) {
			estado = new EstadoValidacion(EstadoValidacion.EXPIRADO, null);
		}
		catch (JWTVerificationException exception){
		    return estado;
		}
		return estado;
	}
	
	/***
	 * Calcula la fecha y hora del momento de ejecución y la desplaza el tiempo que queremos el
	 * token válido.
	 * @return
	 */
	public static Date getExpirationDate() {
		Date fecha = new Date();
		fecha = new Date(fecha.getTime()+DURACION_TOKEN);
		return fecha;
	}
	
}
