package com.selfservice.gestionBD;
/***
 * Clase para implementar el control de acceso, sirve para verificar que un un recurso puede
 * ser accedido por un usuario, concreto. El idCliente del token y el propietaio del recurso
 * a acceder tienen que coincidir.
 * @author doubleg
 *
 */
public class EstadoValidacion {
	public static final int VALIDO = 1;
	//Este nivel de validación permite acceder a cualquier pedido.
	public static final int MAESTRO = 2;
	public static final int EXPIRADO = -2;
	public static final int NO_VALIDO = -3;
	
	private String idUsuarioValido;
	private int estado;
	
	public EstadoValidacion(int estado, String idUsuario) {
		this.estado = estado;
		this.idUsuarioValido = idUsuario;
	}
	
	public int validez() {
		return estado;
	}
	
	/***
	 * Devuelve verdadero si el token es válido y admás válido apra el usuario especificado
	 * @param idCliente
	 * @return
	 */
	public boolean validez(String idCliente) {
		if(idUsuarioValido==null)
			return false;
		
		return estado == 1 &&
				idCliente.equalsIgnoreCase(idUsuarioValido)
				;
	}
	public String getIdUsuario() {
		return idUsuarioValido;
	}
}
