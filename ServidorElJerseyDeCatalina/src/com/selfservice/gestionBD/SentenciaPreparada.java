package com.selfservice.gestionBD;

public enum SentenciaPreparada {
	
	CLIENTE_POR_ID("SELECT "
			+ "idCliente, nombre, apellido, correo, telefono, passwordShadow, passwordSalt "
			+ "FROM cliente "
			+ "WHERE (cliente.idCliente=?);"),
	
	PEDIDO_POR_ID("SELECT idPedido, fechaHora, fechaFinal, estadoPago, estado, " + 
			"idCliente, idCafeteria " + 
			"FROM pedido WHERE (pedido.idPedido=?) "
			+ "AND (pedido.idCliente=?);"),
	
	PEDIDO_POR_IDCAFETERIA("SELECT idPedido, fechaHora, fechaFinal, estadoPago, estado, " + 
			"idCliente, idCafeteria " + 
			"FROM pedido WHERE (pedido.idCafeteria=?);"),
	
	CANTIDAD_POR_IDPEDIDO("SELECT idProducto, cantidad FROM cantidad WHERE (idPedido=?);"),
	
	PEDIDO_POR_IDCLIENTE("SELECT idPedido, fechaHora, fechaFinal, estadoPago, estado, " + 
			"idCliente, idCafeteria " + 
			"FROM pedido WHERE (pedido.idCliente=?);"),
	
	CAFETERIAS("SELECT idCafeteria, nombreCafeteria, horario, "
			+ "coordenadaNorte, coordenadaEste FROM cafeteria WHERE 1;"),
	
	
	// TODO: PRODUCTOS_POR_IDCAFETERIA()
	PRODUCTO_POR_IDCAFETERIA("SELECT "
			+ "producto.idProducto, tipo, nombreProducto, costeUnitario "
			+ "FROM disponibilidad, producto "
			+ "WHERE (disponibilidad.idCafeteria=?) "
			+ "AND (disponibilidad.idProducto=producto.idProducto);"),
	
	INSERTAR_CLIENTE("INSERT INTO cliente (idCliente, nombre, apellido, correo, telefono, passwordShadow, passwordSalt)\n" + 
			"VALUES \n" + 
			//"('julius','Juli','Mejía','gfa@de.alex',SHA2(CONCAT('AAAA', 'secret'),256),656565,'971564528');"
			"(?, ?, ?, ?, ?, ?, ?);"),
	
	INSERTAR_PEDIDO("INSERT INTO pedido ( fechaHora, fechaFinal, estadoPago, estado, idCafeteria, idCliente) "
			+"VALUES                    (    ?,         ?,           ?,        ?,         ?,          ?)"),
	
	INSERTAR_CANTIDAD("INSERT INTO cantidad (idPedido, idProducto, cantidad) "
			+"VALUES (?,?,?)"),
	
	ACTUALIZAR_PEDIDO("UPDATE pedido SET fechaFinal=?, estado=? WHERE pedido.idPedido=?;")
	;

    private final String text;

    /**
     * @param text
     */
    SentenciaPreparada(final String text) {
        this.text = text;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}
